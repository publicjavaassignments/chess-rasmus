package Pieces;

import javafx.scene.paint.Color;

public class Bishop extends Piece{

    private int posX;
    private int posY;

    public Bishop(String imgUrl, Color color, int posX, int posY) {
        super(imgUrl, color);
        this.posX = posX;
        this.posY = posY;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

}
