package Pieces;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;

public class Piece {

    private String imageUrl;
    private Color color;

    public Piece() {

    }

    public Piece(String imageUrl, Color color) {
        this.imageUrl = imageUrl;
        this.color = color;
    }

    public boolean ifWhite() {
        if(this.color.equals(Color.WHITE))
            return true;
        else
            return false;
    }

    /**
     * Create piece
     * */
    public ImageView setPiece() {
        ImageView imageView = new ImageView();
        Image image = new Image(getImageUrl(), 75, 75, true, true);
        imageView.setImage(image);
        return imageView;
    }

    public String getImageUrl() {
        return imageUrl;
    }

}
