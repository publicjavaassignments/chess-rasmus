public class Player {

    private int playerPoint;
    private int playerNumber;
    private String playerName;

    public void setPlayerPoint(int playerPoint) {
        this.playerPoint = playerPoint;
    }

    public void setPlayerNumber(int playerNumber) {
        this.playerNumber = playerNumber;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getPlayerPoint() {
        return playerPoint;
    }

    public int getPlayerNumber() {
        return playerNumber;
    }

    public String getPlayerName() {
        return playerName;
    }
}
