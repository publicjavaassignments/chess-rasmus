package Board;

import Pieces.*;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;



public class Board {

    public static final int SQUARE_AMOUNT = 8;
    public static final int SQUARE_SIZE = 75;
    private Rectangle[][] squares = new Rectangle[SQUARE_AMOUNT][SQUARE_AMOUNT];


    /**
     * Draw each square and place it on the GridPane.
     * */
    public void drawBoard(GridPane gridpane) {
        for (int i = 0; i < SQUARE_AMOUNT; i++) {
            for (int j = 0; j < SQUARE_AMOUNT; j++) {
                Rectangle square = new Rectangle();
                square.setX(i*SQUARE_SIZE);
                square.setY(j*SQUARE_SIZE);

                Color squareColor;
                if((i+j) % 2 == 0) {
                    squareColor = Color.WHITE;
                } else {
                    squareColor = Color.web("6B6B6B");
                }
                square.setFill(squareColor);
                gridpane.add(square, i , j);
                square.widthProperty().bind(gridpane.widthProperty().divide(SQUARE_AMOUNT));
                square.heightProperty().bind(gridpane.heightProperty().divide(SQUARE_AMOUNT));

                squares[i][j] = square;
            }
        }
    }

    /**
     * Place each piece on the board.
     * */
    public void drawPiece(GridPane gridPane) {

        //Set black pieces
        //Rook
        Rook leftRookBlack = new Rook("Resources/rook_black.png", Color.BLACK, 0,0);
        gridPane.add(leftRookBlack.setPiece(), leftRookBlack.getPosX(), leftRookBlack.getPosY());

        Rook rightRookBlack = new Rook("Resources/rook_black.png", Color.BLACK,7, 0);
        gridPane.add(rightRookBlack.setPiece(), rightRookBlack.getPosX(), rightRookBlack.getPosY());

        //Knight
        Knight leftKnightBlack = new Knight("Resources/knight_black.png", Color.BLACK, 1, 0);
        gridPane.add(leftKnightBlack.setPiece(), leftKnightBlack.getPosX(), leftKnightBlack.getPosY());

        Knight rightKnightBlack = new Knight("Resources/knight_black.png", Color.BLACK, 6, 0);
        gridPane.add(rightKnightBlack.setPiece(), rightKnightBlack.getPosX(), rightKnightBlack.getPosY());

        //Bishop
        Bishop leftBishopBlack = new Bishop("Resources/bishop_black.png", Color.BLACK, 2, 0);
        gridPane.add(leftBishopBlack.setPiece(), leftBishopBlack.getPosX(), leftBishopBlack.getPosY());

        Bishop rightBishopBlack = new Bishop("Resources/bishop_black.png", Color.BLACK, 5, 0);
        gridPane.add(rightBishopBlack.setPiece(), rightBishopBlack.getPosX(), rightBishopBlack.getPosY());

        //Queen
        Queen queenBlack = new Queen("Resources/queen_black.png", Color.BLACK, 4, 0);
        gridPane.add(queenBlack.setPiece(), queenBlack.getPosX(), queenBlack.getPosY());

        //King
        King kingBlack = new King("Resources/king_black.png", Color.BLACK, 3, 0);
        gridPane.add(kingBlack.setPiece(), kingBlack.getPosX(), kingBlack.getPosY());

        //pawn
        for(int i = 0; i < 8; i++) {
            Pawn pawnBlack = new Pawn("Resources/pawn_black.png", Color.BLACK, i, 1);
            gridPane.add(pawnBlack.setPiece(), pawnBlack.getPosX(), pawnBlack.getPosY());
        }

        //Set white pieces
        //rook
        Rook leftRookWhite = new Rook("Resources/rook_white.png", Color.WHITE, 0, 7);
        gridPane.add(leftRookWhite.setPiece(), leftRookWhite.getPosX(), leftRookWhite.getPosY());

        Rook rightRookWhite = new Rook("Resources/rook_white.png", Color.WHITE, 7, 7);
        gridPane.add(rightRookWhite.setPiece(), rightRookWhite.getPosX(), rightRookWhite.getPosY());

        //Knight
        Knight leftKnightWhite = new Knight("Resources/knight_white.png", Color.WHITE, 1, 7);
        gridPane.add(leftKnightWhite.setPiece(), leftKnightWhite.getPosX(), leftKnightWhite.getPosY());

        Knight rightKnightWhite = new Knight("Resources/knight_white.png", Color.WHITE, 6, 7);
        gridPane.add(rightKnightWhite.setPiece(), rightKnightWhite.getPosX(), rightKnightWhite.getPosY());

        //Bishop
        Bishop leftBishopWhite = new Bishop("Resources/bishop_white.png", Color.WHITE, 5, 7);
        gridPane.add(leftBishopWhite.setPiece(), leftBishopWhite.getPosX(), leftBishopWhite.getPosY());

        Bishop rightBishopWhite = new Bishop("Resources/bishop_white.png", Color.WHITE, 2, 7);
        gridPane.add(rightBishopWhite.setPiece(), rightBishopWhite.getPosX(), rightBishopWhite.getPosY());

        //Queen
        Queen queenWhite = new Queen("Resources/queen_white.png", Color.WHITE, 4, 7);
        gridPane.add(queenWhite.setPiece(), queenWhite.getPosX(), queenWhite.getPosY());

        //King
        King kingWhite = new King("Resources/king_white.png", Color.WHITE, 3, 7);
        gridPane.add(kingWhite.setPiece(), kingWhite.getPosX(), kingWhite.getPosY());

        //pawn
        for(int i = 0; i < 8; i++) {
            Pawn pawnWhite = new Pawn("Resources/pawn_white.png", Color.WHITE, i, 6);
            gridPane.add(pawnWhite.setPiece(), pawnWhite.getPosX(), pawnWhite.getPosY());
        }
    }


    /**
     * Get selected square from the squares[][]
     */
    public Rectangle getSquare(double xPos, double yPos) {
        Rectangle square = new Rectangle();
        for(int i = 0; i < 8; i++) {
            for(int j = 0; j <8; j++) {
                if(squares[i][j].getX() <= xPos && squares[i][j].getY() <= yPos) {
                    square = squares[i][j];
                }
            }
        }
        return square;
    }
}
