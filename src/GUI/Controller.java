package GUI;

import Board.Board;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Controller {

    public GridPane gridpane;
    public Canvas canvas;
    Board board;

    public void initialize() {
        board = new Board();
        board.drawBoard(gridpane);
        board.drawPiece(gridpane);
    }


    /**
     * Clear marked outline when mouse is released.
     */
    public void clearMarker(MouseEvent e) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.clearRect(0, 0, 600, 600);
    }

    /**
     * Draw outline on selected square
     */
    @FXML
    private void drawMarker(MouseEvent e) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setStroke(Color.RED);

        double x = e.getX();
        double y = e.getY();
        Rectangle square = board.getSquare(x, y);
        gc.strokeRect(square.getX(), square.getY(), Board.SQUARE_SIZE, Board.SQUARE_SIZE);
    }

}